//
//  YBDailymotionAdsAdapter.swift
//  YouboraDailymotionAdapter
//
//  Created by Elisabet Massó on 23/2/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import YouboraLib
import DailymotionPlayerSDK

public class YBDailymotionAdsAdapter: YBPlayerAdapter<AnyObject>, DMPlayerViewControllerDelegate {
    
    private var delegate: DMPlayerViewControllerDelegate?
    
    private var adPlayhead: Double?
    private var adDuration: Double?

    private var firstQuartileTriggered = false
    private var secondQuartileTriggered = false
    private var thirdQuartileTriggered = false
    
    // We must override this init in order to add our init (happens because of interopatability of youbora objc framework with swift).
    private override init() {
        super.init()
    }
    
    public init(player: DMPlayerViewController, delegate: DMPlayerViewControllerDelegate?) {
        super.init(player: player)
        self.delegate = delegate
        registerListeners()
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? DMPlayerViewController {
            player.delegate = self
        }
        resetValues()
    }
    
    public override func unregisterListeners() {
        super.unregisterListeners()
        delegate = nil
        resetValues()
    }
    
    func resetValues() {
        adPlayhead = nil
        adDuration = nil
        firstQuartileTriggered = false
        secondQuartileTriggered = false
        thirdQuartileTriggered = false
    }
    
    // MARK: - Getters
    
    public override func getDuration() -> NSNumber? {
        guard let adDuration = adDuration else { return nil }
        return NSNumber(floatLiteral: adDuration)
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let adPlayhead = adPlayhead else { return nil }
        return NSNumber(floatLiteral: adPlayhead)
    }
    
    public override func getPosition() -> YBAdPosition {
        if flags.joined {
            return YBAdPosition.mid
        }
        return plugin != nil ? YBAdPosition.pre : YBAdPosition.unknown
    }
    
    public override func fireStop(_ params: [String : String]?) {
        resetValues()
        super.fireStop(params)
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdsAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdsAdapterVersion()
    }
    
    // MARK: - DMPlayerViewControllerDelegate
    
    public func player(_ player: DMPlayerViewController, didReceiveEvent event: PlayerEvent) {
        switch event {
            case .timeEvent(name: let name, time: let time):
                if name == "ad_timeupdate" {
                    adPlayhead = time > adDuration ?? 0 ? nil : time
                    if let playhead = adPlayhead, let duration = adDuration {
                        if playhead > duration * 0.25 && !firstQuartileTriggered {
                            fireQuartile(1)
                            firstQuartileTriggered = true
                        } else if playhead > duration * 0.50 && !secondQuartileTriggered {
                            fireQuartile(2)
                            secondQuartileTriggered = true
                        } else if playhead > duration * 0.75 && !thirdQuartileTriggered {
                            fireQuartile(3)
                            thirdQuartileTriggered = true
                        }
                    }
                }
                if name == "ad_durationchange" {
                    adDuration = time
                }
            case .namedEvent(name: let name, data: let data):
                if name == "ad_play" {
                    if flags.joined {
                        if !flags.paused {
                            fireStop(["playhead" : "\(adPlayhead ?? 0)"])
                            fireStart()
                            fireJoin()
                        }
                    }
                    fireJoin()
                    fireResume()
                } else if name == "ad_start" {
                    if let durationString = data?["adData[adDuration]"], let duration = Double(durationString) {
                        adDuration = duration
                    }
                    fireStart()
                } else if name == "ad_pause" {
                    firePause()
                } else if name == "ad_end" {
                    var params = ["playhead" : "\(adPlayhead ?? 0)"]
                    if let playhead = adPlayhead, let duration = adDuration, playhead < duration * 0.95 {
                        params["skipped"] = "true"
                    }
                    fireStop(params)
                } else if name == "gesture_end" {
                    fireStop(["playhead" : "\(adPlayhead ?? 0)"])
                }
            case .errorEvent(error: let error as NSError):
                fireError(withMessage: error.localizedDescription, code: "\(error.code)", andMetadata: nil)
            
        }
        
        delegate?.player(player, didReceiveEvent: event)
        
    }
    
    public func player(_ player: DMPlayerViewController, openUrl url: URL) {
        fireClick(["adUrl" : url.absoluteString])
        delegate?.player(player, openUrl: url)
    }
    
    public func playerDidInitialize(_ player: DMPlayerViewController) {
        delegate?.playerDidInitialize(player)
    }
    
    public func player(_ player: DMPlayerViewController, didFailToInitializeWithError error: Error) {
        fireFatalError(withMessage: nil, code: error.localizedDescription, andMetadata: nil)
        delegate?.player(player, didFailToInitializeWithError: error)
    }

}
