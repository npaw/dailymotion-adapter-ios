//
//  YBDailymotionAdapter.swift
//  YouboraDailymotionAdapter
//
//  Created by Jorge on 13/03/2019.
//  Copyright © 2019 NPAW. All rights reserved.
//

import YouboraLib
import DailymotionPlayerSDK

public class YBDailymotionAdapter: YBPlayerAdapter<AnyObject>, DMPlayerViewControllerDelegate {
    
    private var delegate: DMPlayerViewControllerDelegate?
    
    private var videoTitle: String?
    private var videoRendition: String?
    private var videoDuration: Double?
    private var videoPlayhead: Double?

    // We must override this init in order to add our init (happens because of interopatability of youbora objc framework with swift).
    private override init() {
        super.init()
    }
    
    public init(player: DMPlayerViewController, delegate: DMPlayerViewControllerDelegate?) {
        super.init(player: player)
        self.delegate = delegate
        registerListeners()
    }
    
    public override func registerListeners() {
        super.registerListeners()
        resetValues()
        if let player = player as? DMPlayerViewController {
            player.delegate = self
        }
        monitorPlayhead(withBuffers: true, seeks: false, andInterval: 800)
    }
    
    public override func unregisterListeners() {
        delegate = nil
        monitor?.stop()
        if let adsAdapter =  plugin?.adsAdapter, adsAdapter.isKind(of: YBDailymotionAdsAdapter.self) {
            plugin?.removeAdsAdapter()
        }
        super.unregisterListeners()
    }
    
    func resetValues() {
        videoTitle = nil
        videoRendition = nil
        videoDuration = nil
        videoPlayhead = nil
    }
    
    // MARK: - Getters
    
    public override func getDuration() -> NSNumber? {
        guard let videoDuration = videoDuration, plugin?.options.contentIsLive != NSNumber(value: true) else { return super.getDuration() }
        return NSNumber(floatLiteral: videoDuration)
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let lastPlayhead = videoPlayhead else { return super.getPlayhead() }
        return NSNumber(floatLiteral: lastPlayhead)
    }
    
    public override func getRendition() -> String? {
        guard let videoRendition = videoRendition else { return super.getRendition() }
        return videoRendition
    }
    
    public override func getTitle() -> String? {
        return videoTitle
    }
    
    public override func fireStop() {
        resetValues()
        super.fireStop()
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdapterVersion()
    }
    
    // MARK: - DMPlayerViewControllerDelegate
    
    public func player(_ player: DMPlayerViewController, didReceiveEvent event: PlayerEvent) {
        
        switch event {
            case .timeEvent(name: let name, time: let time):
                if name == "durationchange" {
                    videoDuration = time
                }
                if name == "timeupdate" {
                    if plugin?.adsAdapter == nil || !(plugin?.adsAdapter?.flags.started ?? false) {
                        videoPlayhead = time
                    }
                }
                if name == "seeking" {
                    if flags.joined && time > 0 {
                        fireSeekBegin()
                    }
                } else if name == "seeked" {
                    fireSeekEnd()
                    monitor?.skipNextTick()
                }
            case .namedEvent(name: let name, data: let data):
                if name == "video_start" {
                    fireStop()
                    fireStart()
                } else if name == "play" {
                    fireResume()
                    fireStart()
                } else if name == "pause" {
                    firePause()
                } else if name == "playing" {
                    fireJoin()
                    monitor?.skipNextTick()
                } else if name == "video_end" {
                    fireStop()
                    videoPlayhead = 0
                }
                if name == "videochange" {
                    if let title = data?["title"] {
                        videoTitle = title
                    }
                }
                if name == "qualitychange" {
                    if let quality = data?["quality"] {
                        if videoRendition != nil {
                            monitor?.skipNextTick()
                        }
                        videoRendition = quality
                    }
                }
                if name == "error" {
                    if let errorTitle = data?["title"], let errorCode = data?["code"] {
                        fireError(withMessage: errorTitle, code: errorCode, andMetadata: nil)
                        fireStop()
                    } else {
                        fireError(nil)
                    }
                }
            case .errorEvent(error: let error as NSError):
                fireError(withMessage: error.localizedDescription, code: "\(error.code)", andMetadata: nil)
                
        }
        
        delegate?.player(player, didReceiveEvent: event)
        
    }
    
    public func player(_ player: DMPlayerViewController, openUrl url: URL) {
        delegate?.player(player, openUrl: url)
    }
    
    public func playerDidInitialize(_ player: DMPlayerViewController) {
        delegate?.playerDidInitialize(player)
    }
    
    public func player(_ player: DMPlayerViewController, didFailToInitializeWithError error: Error) {
        fireFatalError(withMessage: nil, code: error.localizedDescription, andMetadata: nil)
        delegate?.player(player, didFailToInitializeWithError: error)
    }
}
