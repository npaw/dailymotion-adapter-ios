Pod::Spec.new do |s|

  s.name         = 'YouboraDailymotionAdapter'
  s.version      = '6.5.2'

  # Metadata
  s.summary      = 'Adapter to use YouboraLib on DailymotionPlayer'

  s.description  = <<-DESC
                        YouboraDailymotionAdapter is an adapter used for DailymotionPlayer.
                      DESC

  s.homepage     = 'http://developer.nicepeopleatwork.com/'

  s.license      = { :type => 'MIT', :file => 'LICENSE.md' }

  s.author             = { 'Nice People at Work' => 'support@nicepeopleatwork.com' }

  # Platforms
  s.ios.deployment_target = '9.0'

  s.swift_version = '4.0', '4.1', '4.2', '4.3', '5.0', '5.1', '5.2', '5.3'

  # Source Location
  s.source       = { :git => 'https://bitbucket.org/npaw/dailymotion-adapter-ios.git', :tag => s.version }

  # Source files
  s.source_files  = 'YouboraDailymotionAdapter/**/*.{h,m,swift}'
  
  s.public_header_files = 'YouboraDailymotionAdapter/**/*.h'

  # Project settings
  s.requires_arc = true
  s.pod_target_xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => '$(inherited) YOUBORADAILYMOTIONADAPTER_VERSION=' + s.version.to_s }
  
  # Dependency
  s.dependency 'YouboraLib', '~> 6.5'
  s.dependency 'DailymotionPlayerSDK', '~> 3.7'

end
