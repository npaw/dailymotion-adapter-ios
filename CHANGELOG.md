## [6.5.2] - 2021-06-04
### Fixed
- Reported buffer when changing rendition

## [6.5.1] - 2021-05-27
### Added
- Changelog
- Lib constants

### Fixed
- Adapter version

## [6.5.0] - 2021-04-21
### Added
- Release version