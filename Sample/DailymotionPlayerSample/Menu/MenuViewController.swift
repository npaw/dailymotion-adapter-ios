//
//  MenuViewController.swift
//  DailymotionPlayerSample
//
//  Created by Elisabet Massó on 13/4/21.
//

import UIKit
import YouboraLib
import YouboraConfigUtils

class MenuViewController: UIViewController {
    
    @IBOutlet weak var resourceTf: UITextField!
//    @IBOutlet weak var adsSwitch: UISwitch!
    @IBOutlet weak var playBtn: UIButton!
    
    var plugin: YBPlugin?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        YBLog.setDebugLevel(.debug)
        
        addSettingsButton()
        
        resourceTf.text = AdResource.dailymotionDemonstration
//        adsSwitch.isOn = false
    }
    
    public static func initFromXIB() -> MenuViewController? {
        return MenuViewController(nibName: String(describing: MenuViewController.self), bundle: Bundle(for: MenuViewController.self))
    }
    
}

// MARK: - Settings Section
extension MenuViewController {
    
    func addSettingsButton() {
        guard let navigationController = self.navigationController else { return }
        addSettingsToNavigation(navigationBar: navigationController.navigationBar)
    }
    
    func addSettingsToNavigation(navigationBar: UINavigationBar) {
        let settingsButton = UIBarButtonItem(title: "Settings", style: .done, target: self, action: #selector(navigateToSettings))
        navigationBar.topItem?.rightBarButtonItem = settingsButton
    }
}

// MARK: - Navigation Section
extension MenuViewController {
    
    @IBAction func sendOfflineEventsButtonPressed(_ sender: UIButton) {
        let options = YouboraConfigManager.getOptions()
        options.offline = false
        
        self.plugin = YBPlugin(options: options)
        
        for _ in 1...3 {
            self.plugin?.fireOfflineEvents()
        }
    }
    
    @IBAction func playButtonPressed(_ sender: UIButton) {
        if sender == playBtn {
            guard let playerViewController = PlayerViewController.initFromXIB() else { return }
            
            playerViewController.resource = self.resourceTf.text
            playerViewController.containAds = false // self.adsSwitch.isOn
            
            navigateToViewController(viewController: playerViewController)
            return
        }
    }
    
    @objc func navigateToSettings() {
        guard let _ = self.navigationController else {
            navigateToViewController(viewController: YouboraConfigViewController.initFromXIB(animatedNavigation: false))
            return
        }
        
        navigateToViewController(viewController: YouboraConfigViewController.initFromXIB())
    }
    
    func navigateToViewController(viewController: UIViewController) {
        guard let navigationController = self.navigationController else {
            self.present(viewController, animated: true, completion: nil)
            return
        }
        
        navigationController.pushViewController(viewController, animated: true)
    }
    
}
