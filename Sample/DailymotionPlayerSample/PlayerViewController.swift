//
//  PlayerViewController.swift
//  DailymotionPlayerSample
//
//  Created by Elisabet Massó on 13/4/21.
//

import UIKit
import AVKit
import SafariServices
import DailymotionPlayerSDK
import YouboraLib
import YouboraConfigUtils
import YouboraDailymotionAdapter

class PlayerViewController: UIViewController {

    public static func initFromXIB() -> PlayerViewController? {
        return PlayerViewController(nibName: String(describing: PlayerViewController.self), bundle: Bundle(for: PlayerViewController.self))
    }
    
    var resource: String?
    var containAds: Bool?
    
    var plugin: YBPlugin?
    
    var adsTimer: Timer?
    var currentTime: Double?
    var currentAdPosition = 0
    var currentAdLink: String?
    
    private var adsInterval: [Int] {
        guard let containAds = self.containAds else { return  [] }
        if containAds { return [10,20,30] }
        return []
    }
    
    var changeItemButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Change Item", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(changeItem), for: .touchUpInside)
        return button
    }()
    
    var buttonReplay: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Replay", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(pressToReply), for: .touchUpInside)
        return button
    }()
    
    @IBOutlet private var containerView: UIView!
    @IBOutlet fileprivate var playerHeightConstraint: NSLayoutConstraint! {
        didSet {
            initialPlayerHeight = playerHeightConstraint.constant
        }
    }
    fileprivate var initialPlayerHeight: CGFloat!
    fileprivate var isPlayerFullscreen = false
    
    fileprivate lazy var playerViewController: DMPlayerViewController = {
        let parameters: [String: Any] = [
            "fullscreen-action": "trigger_event",
            "sharing-action": "trigger_event",
            "mute": "true"
        ]
        let controller = DMPlayerViewController(parameters: parameters, allowIDFA: true)
        controller.delegate = self
        return controller
    }()
    
    fileprivate var adsPlayerViewController: DMPlayerViewController?
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterForeground(_:)), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        initializeYoubora()
        setupPlayerViewController()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback)
            try audioSession.setActive(true)
        }
        catch {
          print("Setting category to AVAudioSessionCategoryPlayback failed.")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let adsTimer = adsTimer {
            adsTimer.invalidate()
            self.adsTimer = nil
        }
        
        if isMovingFromParent || isBeingDismissed {
            plugin?.fireStop()
            plugin?.removeAdapter()
            plugin?.removeAdsAdapter()
        }
    }
    
    private func initializeYoubora() {
        YBLog.setDebugLevel(.debug)
        
        let options = YouboraConfigManager.getOptions()
        
        options.contentIsLive = NSNumber(value: resource == Resource.live || resource == Resource.liveAirShow)
        options.contentResource = self.resource
        plugin = YBPlugin(options: options)
        
        // Initialize player on this view controller
        initializePlayer()
        initializeAds()
    }
    
    private func setupPlayerViewController() {
        containerView.superview?.addSubview(changeItemButton)
        NSLayoutConstraint.activate([
            changeItemButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 80),
            changeItemButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            changeItemButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            changeItemButton.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        containerView.superview?.addSubview(buttonReplay)
        NSLayoutConstraint.activate([
            buttonReplay.topAnchor.constraint(equalTo: changeItemButton.bottomAnchor, constant: 20),
            buttonReplay.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
            buttonReplay.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
            buttonReplay.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        addChild(playerViewController)
        
        let view = playerViewController.view!
        containerView.addSubview(view)
        view.usesAutolayout(true)
        NSLayoutConstraint.activate([
            view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            view.topAnchor.constraint(equalTo: containerView.topAnchor),
            view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        ])
    }
    
    @objc func changeItem() {
        guard let newResource = [Resource.dailymotionLandscape, Resource.dailymotionParty ].randomElement() else { return }
        self.plugin?.options.contentIsLive = NSNumber(false)
        playerViewController.load(videoId: getVideoId(from: newResource))
    }
    
    @objc func pressToReply() {
        playerViewController.seek(to: .zero)
        currentTime = 0
        playerViewController.play()
    }
    
    @objc func didEnterBackground(_ notification: Notification?) {
        //
    }
    
    @objc func didEnterForeground(_ notification: Notification?) {
        playerViewController.play()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        isPlayerFullscreen = size.width > size.height
        playerViewController.toggleFullscreen()
        updatePlayer(height: size.height)
    }

}

// MARK: - Video Player methods
extension PlayerViewController {
    
    private func initializePlayer() {
        guard let resource = self.resource else { return }
        
        plugin?.adapter = YBDailymotionAdapter(player: playerViewController, delegate: self)

        // Add resource
        playerViewController.load(videoId: getVideoId(from: resource))
        
        // Start playback
        playerViewController.play()
    }
    
    func getVideoId(from resource: String) -> String {
        if let lastIndex = resource.lastIndex(of: "/"), let index = resource.index(lastIndex, offsetBy: 1, limitedBy: resource.index(before: resource.endIndex)) {
            let lastWord = resource[index...]
            return String(lastWord)
        }
        return resource
    }
    
}

// MARK: - Ads Methods
extension PlayerViewController {
    func initializeAds() {
        guard let containAds = containAds else {
            return
        }
        
        if containAds {
            self.adsTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkAdsToBePlayed), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc func checkAdsToBePlayed() {
        guard let currentPlayhead = currentTime else {
            return
        }
        
        if currentAdLink != nil { return }
        
        if (currentAdPosition > (adsInterval.count - 1)) { return }
        
        if CMTimeGetSeconds(CMTime(seconds: currentPlayhead, preferredTimescale: 1)) < Double(adsInterval[currentAdPosition]) {
            return
        }
        
        currentAdPosition = currentAdPosition + 1
        
        currentAdLink = AdResource.dailymotionDemonstration
                
        playerViewController.pause()
        
        // Video player controller
        let parameters: [String: Any] = [
            "fullscreen-action": "trigger_event",
            "sharing-action": "trigger_event",
            "mute": "true",
            "controls": "false"
        ]
        adsPlayerViewController = DMPlayerViewController(parameters: parameters, allowIDFA: true)
        adsPlayerViewController?.delegate = self
        
        // Add view to the current screen
        addChild(adsPlayerViewController!)
        view.addSubview(adsPlayerViewController!.view)
        
        // We use the playerView view as a guide for the video
        adsPlayerViewController?.view.frame = view.frame
        
        // Manage player
        adsPlayerViewController?.load(videoId: getVideoId(from: currentAdLink!))
        plugin?.options.adResource = currentAdLink
        plugin?.adsAdapter = YBDailymotionAdsAdapter(player: adsPlayerViewController!, delegate: self)

        // Start playback
        adsPlayerViewController?.play()
        
    }
    
    func adDidFinish() {
        adsPlayerViewController?.view.removeFromSuperview()
        adsPlayerViewController?.removeFromParent()
        adsPlayerViewController = nil
        
        playerViewController.play()
        
        currentAdLink = nil
        plugin?.removeAdsAdapter()
    }
    
}

extension PlayerViewController: DMPlayerViewControllerDelegate {
    
    func player(_ player: DMPlayerViewController, didReceiveEvent event: PlayerEvent) {
        
        switch event {
            case .timeEvent(name: let name, time: let time) where name == "timeupdate":
                currentTime = time
            case .namedEvent(let name, _) where name == "fullscreen_toggle_requested":
                toggleFullScreen()
            case .namedEvent(let name, .some(let data)) where name == "share_requested":
                guard let raw = data["shortUrl"] ?? data["url"], let url = URL(string: raw) else { return }
                share(url: url)
                
            /// Forced ad for testing
            case .namedEvent(let name, _) where name == "video_end":
                if plugin?.adsAdapter != nil && currentAdLink != nil {
                    adDidFinish()
                }

            /// Instantiate the adsAdapter and call fireStart()
            case .namedEvent(let name, _) where name == "ad_start":
                if plugin?.adsAdapter == nil {
                    plugin?.removeAdapter()
                    plugin?.adsAdapter = YBDailymotionAdsAdapter(player: playerViewController, delegate: self)
                    plugin?.adsAdapter?.fireStart()
                }
            /// Remove the adsAdapter when the ads have finished
            case .namedEvent(let name, _) where name == "ad_end" || name == "gesture_end":
                if plugin?.adsAdapter != nil {
                    plugin?.removeAdsAdapter()
                    plugin?.adapter = YBDailymotionAdapter(player: playerViewController, delegate: self)
                }
                
            default:
                break
        }
    }
    
    fileprivate func toggleFullScreen() {
        isPlayerFullscreen = !isPlayerFullscreen
        updateDeviceOrientation()
        updatePlayer(height: view.frame.size.height)
    }
    
    private func updateDeviceOrientation() {
        let orientation: UIDeviceOrientation = isPlayerFullscreen ? .landscapeLeft : .portrait
        UIDevice.current.setValue(orientation.rawValue, forKey: #keyPath(UIDevice.orientation))
    }
    
    fileprivate func updatePlayer(height: CGFloat) {
        if isPlayerFullscreen {
            playerHeightConstraint.constant = height
        } else {
            playerHeightConstraint.constant = initialPlayerHeight
        }
    }
    
    private func share(url: URL) {
        playerViewController.pause()
        let item = ShareActivityItemProvider(title: "Dailymotion", url: url)
        let shareViewController = UIActivityViewController(activityItems: [item], applicationActivities: nil)
        shareViewController.excludedActivityTypes = [.assignToContact, .print]
        present(shareViewController, animated: true, completion: nil)
    }
    
    func player(_ player: DMPlayerViewController, openUrl url: URL) {
        print("openUrl: \(url)")
        let controller = SFSafariViewController(url: url)
        present(controller, animated: true, completion: nil)
    }
    
    func playerDidInitialize(_ player: DMPlayerViewController) {
        print("playerDidInitialize")
    }
    
    func player(_ player: DMPlayerViewController, didFailToInitializeWithError error: Error) {
        print("didFailToInitializeWithError: \(error.localizedDescription)")
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true)
    }
    
}
